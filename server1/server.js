const http = require('http');
const url = require('url');

//create a server object:
http.createServer(function (req, res) {
  const queryObject = JSON.stringify(url.parse(req.url,true).query);
  res.write('Hello World Server 1 ! Params : ' + queryObject); //write a response to the client
  res.end(); //end the response
}).listen(3000); //the server object listens on port 3000 